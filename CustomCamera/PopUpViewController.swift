//
//  PopUpViewController.swift
//  CustomCamera
//
//  Created by MacBook Pro TI on 24/08/18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController {

    @IBOutlet weak var counterImg: UIImageView!
    var countdownTimer: Timer!
    var totalTime = 6
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        self.showAnimate()
        startTimer()
        // Do any additional setup after loading the view.
    }

    
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    func updateTime() {
        
        if totalTime != 0 {
            totalTime -= 1
            switch(totalTime){
            case 5:
                counterImg.image = #imageLiteral(resourceName: "55")
                break
            case 1:
                counterImg.image = #imageLiteral(resourceName: "11")
                break
            case 2:
                counterImg.image = #imageLiteral(resourceName: "22")
                break
            case 3:
                counterImg.image = #imageLiteral(resourceName: "33")
                break
            case 4:
                counterImg.image = #imageLiteral(resourceName: "44")
                break
            case 0:
                counterImg.image = #imageLiteral(resourceName: "0")
                self.removeAnimate()
                break
            default:
                counterImg.image = nil
                break
            }
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        //return String(format: "%02d:%02d", minutes, seconds)
        return String(totalSeconds)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func closePopUp(_ sender: AnyObject) {
        self.removeAnimate()
        //self.view.removeFromSuperview()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }

}
