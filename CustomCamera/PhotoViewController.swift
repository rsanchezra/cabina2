//
//  PhotoViewController.swift
//  CustomCamera
//
//  Created by Brian Advent on 24/01/2017.
//  Copyright © 2017 Brian Advent. All rights reserved.
//

import UIKit
import ImageIO

class PhotoViewController: UIViewController, UIViewControllerTransitioningDelegate {

    var takenPhoto:UIImage?
    
    @IBOutlet weak var marco_select: UIImageView!
    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var fondo4: UIImageView!
    @IBOutlet weak var lineas4: UIImageView!
    @IBOutlet weak var marco1_m: UIImageView!
    @IBOutlet weak var marco2_m: UIImageView!
    @IBOutlet weak var marco3_m: UIImageView!
    @IBOutlet weak var marco4_m: UIImageView!
    @IBOutlet weak var marco5_m: UIImageView!
    @IBOutlet weak var siguiente5: UIImageView!
    @IBOutlet weak var btnSiguiente5: UIButton!
    let transition = CircularTransition()
    
    @IBOutlet weak var s1: UIImageView!
    @IBOutlet weak var s2: UIImageView!
    @IBOutlet weak var s3: UIImageView!
    @IBOutlet weak var s4: UIImageView!
    @IBOutlet weak var s5: UIImageView!
    @IBOutlet weak var marco_3m: UIImageView!
    
    var cont1 = 0;
    var cont2 = 0;
    var cont3 = 0;
    var cont4 = 0;
    var cont5 = 0;
    
    
    
    
    @IBAction func btnRegresar(_ sender: Any) {
        goBack()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let availableImage = takenPhoto {
            
            var transform = CGAffineTransform.identity
            

            
            switch UIApplication.shared.statusBarOrientation{

                case .unknown:
                    print("SABE")
                break
                case .landscapeLeft:
                    print("IZQUIERDA")
                    transform = transform.translatedBy(x: 0.0, y: 0.0)
                    transform = transform.rotated(by: .pi / 1.0)
                    transform = transform.translatedBy(x: 0.0, y: 0.0)
                    transform = transform.rotated(by: .pi / 1.0);
                    let cgImg = availableImage.cgImage
                    
                    if let context = CGContext(data: nil,
                                               width: 1280, height: 960,
                                               bitsPerComponent: (cgImg?.bitsPerComponent)!,
                                               bytesPerRow: 0, space: (cgImg?.colorSpace!)!,
                                               bitmapInfo: (cgImg?.bitmapInfo.rawValue)!) {
                        
                        context.concatenate(transform)
                        
                        if availableImage.imageOrientation == .left || availableImage.imageOrientation == .leftMirrored ||
                            availableImage.imageOrientation == .right || availableImage.imageOrientation == .rightMirrored {
                            context.draw(cgImg!, in: CGRect(x: 0.0, y: 0.0, width: 1280, height: 960))
                        } else {
                            context.draw(cgImg!, in: CGRect(x: 0.0 , y: 0.0, width: 1280, height: 960))
                        }
                        
                        if let contextImage = context.makeImage() {
                            ImageView.image = UIImage(cgImage: contextImage)
                            let imageData:NSData = UIImagePNGRepresentation(takenPhoto!)! as NSData
                            Constants.strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
                        }
                        
                    }
                    break
                case .landscapeRight:
                    ImageView.image = availableImage
                            self.ImageView.transform = CGAffineTransform(rotationAngle: .pi*0.5)
                    ImageView.frame = CGRect(x: 104, y: 83, width: 780, height: 420)
                    let imageData:NSData = UIImagePNGRepresentation(ImageView.image!)! as NSData
                    Constants.strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
                    Constants.orientacion = "mal"
                    
                    
                    break
                case .portrait:
                    print("P")
                break
                case .portraitUpsideDown:
                    print("PUD")
                break
                default: break
            }
            
            
            

            
        }
        fondo4.image = #imageLiteral(resourceName: "fondo4")
        lineas4.image = #imageLiteral(resourceName: "lineas4")
        marco1_m.image = #imageLiteral(resourceName: "marco1_m")
        marco2_m.image = #imageLiteral(resourceName: "marco2_m")
        marco3_m.image = #imageLiteral(resourceName: "marco3_m")
        marco_3m.image = #imageLiteral(resourceName: "marco3_m")
        marco4_m.image = #imageLiteral(resourceName: "marco4_m")
        marco5_m.image = #imageLiteral(resourceName: "marco5_m")
        marco_select.image = #imageLiteral(resourceName: "marco3_g")
        siguiente5.image = nil
        s1.image = nil
        s2.image = nil
        s3.image = nil
        s4.image = nil
        s5.image = nil
        
//        UIView.animate(withDuration: 2.0, animations: {
  //          self.imageView.transform = CGAffineTransform(rotationAngle: (90.0 * .pi) / 90.0)
    //    })
        
        let tapmarco1 = UITapGestureRecognizer(target: self, action: #selector(marco_select1))
        marco1_m.addGestureRecognizer(tapmarco1)
        marco1_m.isUserInteractionEnabled = true
        
        let tapmarco2 = UITapGestureRecognizer(target: self, action: #selector(marco_select2))
        marco2_m.addGestureRecognizer(tapmarco2)
        marco2_m.isUserInteractionEnabled = true
        
        let tapmarco3 = UITapGestureRecognizer(target: self, action: #selector(marco_select3))
        marco3_m.addGestureRecognizer(tapmarco3)
        marco3_m.isUserInteractionEnabled = true
        
        let tapmarco3m = UITapGestureRecognizer(target: self, action: #selector(marco_select3))
        marco_3m.addGestureRecognizer(tapmarco3m)
        marco_3m.isUserInteractionEnabled = true
        
        let tapmarco4 = UITapGestureRecognizer(target: self, action: #selector(marco_select4))
        marco4_m.addGestureRecognizer(tapmarco4)
        marco4_m.isUserInteractionEnabled = true
        
        let tapmarco5 = UITapGestureRecognizer(target: self, action: #selector(marco_select5))
        marco5_m.addGestureRecognizer(tapmarco5)
        marco5_m.isUserInteractionEnabled = true
        
        
    }
    
    
    func goBack() {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func marco_select1() {
        marco_select.image = #imageLiteral(resourceName: "marco1_g")
        s1.image = #imageLiteral(resourceName: "seleccionado")
        s2.image = nil
        s3.image = nil
        s4.image = nil
        s5.image = nil
        Constants.marco_seleccionado = "1"
        
        let s1x = marco1_m.frame.origin.x
        let s1y = marco1_m.frame.origin.y
        let s2y = marco2_m.frame.origin.y
        let s3y = marco_3m.frame.origin.y
        let s4y = marco4_m.frame.origin.y
        let s5y = marco5_m.frame.origin.y
        let s2x = marco2_m.frame.origin.x
        let s3x = marco_3m.frame.origin.x
        let s4x = marco4_m.frame.origin.x
        let s5x = marco5_m.frame.origin.x
        
        marco1_m.frame = CGRect(x: s1x, y: s1y, width: marco1_m.frame.size.width, height: marco1_m.frame.size.height)
        
        if cont1 == 0 {
            cont1 = cont1 + 1
        
            marco1_m.frame = CGRect(x: s1x, y: s1y-20, width: marco1_m.frame.size.width, height: marco1_m.frame.size.height)
        
            marco2_m.frame = CGRect(x: s2x, y: s2y, width: marco2_m.frame.size.width, height: marco2_m.frame.size.height)
        
            marco_3m.frame = CGRect(x: s3x, y: s3y, width: marco_3m.frame.size.width, height: marco_3m.frame.size.height)
        
            marco4_m.frame = CGRect(x: s4x, y: s4y, width: marco4_m.frame.size.width, height: marco4_m.frame.size.height)
        
            marco5_m.frame = CGRect(x: s5x, y: s5y, width: marco5_m.frame.size.width, height: marco5_m.frame.size.height)
        }
        
        

    }
    
    
    func marco_select2() {
        marco_select.image = #imageLiteral(resourceName: "marco2_g")
        s1.image = nil
        s2.image = #imageLiteral(resourceName: "seleccionado")
        s3.image = nil
        s4.image = nil
        s5.image = nil
        Constants.marco_seleccionado = "2"
        print(Constants.marco_seleccionado)
        
        let s1x = marco2_m.frame.origin.x
        let s1y = marco2_m.frame.origin.y
        let s2y = marco1_m.frame.origin.y
        let s3y = marco_3m.frame.origin.y
        let s4y = marco4_m.frame.origin.y
        let s5y = marco5_m.frame.origin.y
        let s2x = marco1_m.frame.origin.x
        let s3x = marco_3m.frame.origin.x
        let s4x = marco4_m.frame.origin.x
        let s5x = marco5_m.frame.origin.x
        
        marco2_m.frame = CGRect(x: s1x, y: s1y, width: marco2_m.frame.size.width, height: marco2_m.frame.size.height)
        
        if cont2 == 0 {
            cont2 = cont2 + 1
        
            marco2_m.frame = CGRect(x: s1x, y: s1y-20, width: marco2_m.frame.size.width, height: marco2_m.frame.size.height)
        
            marco1_m.frame = CGRect(x: s2x, y: s2y, width: marco1_m.frame.size.width, height: marco1_m.frame.size.height)
        
            marco_3m.frame = CGRect(x: s3x, y: s3y, width: marco_3m.frame.size.width, height: marco_3m.frame.size.height)
        
            marco4_m.frame = CGRect(x: s4x, y: s4y, width: marco4_m.frame.size.width, height: marco4_m.frame.size.height)
        
            marco5_m.frame = CGRect(x: s5x, y: s5y, width: marco5_m.frame.size.width, height: marco5_m.frame.size.height)
        }
    }
    
    
    func marco_select3() {
        marco_select.image = #imageLiteral(resourceName: "marco3_g")
        s1.image = nil
        s2.image = nil
        s3.image = #imageLiteral(resourceName: "seleccionado")
        s4.image = nil
        s5.image = nil
        Constants.marco_seleccionado = "3"
        print(Constants.marco_seleccionado)
        
        let s1x = marco_3m.frame.origin.x
        let s1y = marco_3m.frame.origin.y
        let s2y = marco2_m.frame.origin.y
        let s3y = marco1_m.frame.origin.y
        let s4y = marco4_m.frame.origin.y
        let s5y = marco5_m.frame.origin.y
        let s2x = marco2_m.frame.origin.x
        let s3x = marco1_m.frame.origin.x
        let s4x = marco4_m.frame.origin.x
        let s5x = marco5_m.frame.origin.x
        
        marco_3m.frame = CGRect(x: s1x, y: s1y, width: marco_3m.frame.size.width, height: marco_3m.frame.size.height)
        
        if cont3 == 0 {
            cont3 = cont3 + 1
        
            marco_3m.frame = CGRect(x: s1x, y: s1y-20, width: marco_3m.frame.size.width, height: marco_3m.frame.size.height)
        
            marco2_m.frame = CGRect(x: s2x, y: s2y, width: marco2_m.frame.size.width, height: marco2_m.frame.size.height)
        
            marco1_m.frame = CGRect(x: s3x, y: s3y, width: marco1_m.frame.size.width, height: marco1_m.frame.size.height)
        
            marco4_m.frame = CGRect(x: s4x, y: s4y, width: marco4_m.frame.size.width, height: marco4_m.frame.size.height)
        
            marco5_m.frame = CGRect(x: s5x, y: s5y, width: marco5_m.frame.size.width, height: marco5_m.frame.size.height)
        }
        
    }
    
    
    func marco_select4() {
        marco_select.image = #imageLiteral(resourceName: "marco4_g")
        s1.image = nil
        s2.image = nil
        s3.image = nil
        s4.image = #imageLiteral(resourceName: "seleccionado")
        s5.image = nil
        Constants.marco_seleccionado = "4"
        print(Constants.marco_seleccionado)
        
        let s1x = marco4_m.frame.origin.x
        let s1y = marco4_m.frame.origin.y
        let s2y = marco2_m.frame.origin.y
        let s3y = marco_3m.frame.origin.y
        let s4y = marco1_m.frame.origin.y
        let s5y = marco5_m.frame.origin.y
        let s2x = marco2_m.frame.origin.x
        let s3x = marco_3m.frame.origin.x
        let s4x = marco1_m.frame.origin.x
        let s5x = marco5_m.frame.origin.x
        
        
        marco4_m.frame = CGRect(x: s1x, y: s1y, width: marco4_m.frame.size.width, height: marco4_m.frame.size.height)
        if cont4 == 0 {
            cont4 = cont4 + 1
        
            marco4_m.frame = CGRect(x: s1x, y: s1y-20, width: marco4_m.frame.size.width, height: marco4_m.frame.size.height)
        
            marco2_m.frame = CGRect(x: s2x, y: s2y, width: marco2_m.frame.size.width, height: marco2_m.frame.size.height)
        
            marco_3m.frame = CGRect(x: s3x, y: s3y, width: marco_3m.frame.size.width, height: marco_3m.frame.size.height)
        
            marco1_m.frame = CGRect(x: s4x, y: s4y, width: marco1_m.frame.size.width, height: marco1_m.frame.size.height)
        
            marco5_m.frame = CGRect(x: s5x, y: s5y, width: marco5_m.frame.size.width, height: marco5_m.frame.size.height)
        }
        
    }
    
    
    func marco_select5() {
        marco_select.image = #imageLiteral(resourceName: "marco5_g")
        s1.image = nil
        s2.image = nil
        s3.image = nil
        s4.image = nil
        s5.image = #imageLiteral(resourceName: "seleccionado")
        Constants.marco_seleccionado = "5"
        print(Constants.marco_seleccionado)
        
        let s1x = marco5_m.frame.origin.x
        let s1y = marco5_m.frame.origin.y
        let s2y = marco2_m.frame.origin.y
        let s3y = marco_3m.frame.origin.y
        let s4y = marco4_m.frame.origin.y
        let s5y = marco1_m.frame.origin.y
        let s2x = marco2_m.frame.origin.x
        let s3x = marco_3m.frame.origin.x
        let s4x = marco4_m.frame.origin.x
        let s5x = marco1_m.frame.origin.x
        
        marco5_m.frame = CGRect(x: s1x, y: s1y, width: marco5_m.frame.size.width, height: marco5_m.frame.size.height)
        if cont5 == 0 {
            cont5 = cont5 + 1
            
            marco5_m.frame = CGRect(x: s1x, y: s1y-20, width: marco5_m.frame.size.width, height: marco5_m.frame.size.height)
            
            marco2_m.frame = CGRect(x: s2x, y: s2y, width: marco2_m.frame.size.width, height: marco2_m.frame.size.height)
            
            marco_3m.frame = CGRect(x: s3x, y: s3y, width: marco_3m.frame.size.width, height: marco_3m.frame.size.height)
            
            marco4_m.frame = CGRect(x: s4x, y: s4y, width: marco4_m.frame.size.width, height: marco4_m.frame.size.height)
            
            marco1_m.frame = CGRect(x: s5x, y: s5y, width: marco1_m.frame.size.width, height: marco1_m.frame.size.height)
        }
        
    }
    
    func nextS() {
        let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "correo") as! CorreoViewController
        
        
        
        DispatchQueue.main.async {
            self.present(photoVC, animated: true, completion: nil)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondVC = segue.destination as! CorreoViewController
        secondVC.transitioningDelegate = self
        secondVC.modalPresentationStyle = .custom
    }
    
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = btnSiguiente5.center
        transition.circleColor = hexStringToUIColor(hex: "#0094A8")
        return transition
    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
