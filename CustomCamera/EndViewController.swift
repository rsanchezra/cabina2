//
//  EndViewController.swift
//  CustomCamera
//
//  Created by MacBook Pro TI on 15/08/18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//

import UIKit

class EndViewController: UIViewController {

    @IBOutlet weak var fondo7: UIImageView!
    @IBOutlet weak var logo7: UIImageView!
    @IBOutlet weak var gracias: UIImageView!
    @IBOutlet weak var lineas7: UIImageView!
    @IBOutlet weak var lblNombre: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fondo7.image = #imageLiteral(resourceName: "fondo7")
        logo7.image = #imageLiteral(resourceName: "logo7")
        gracias.image = #imageLiteral(resourceName: "gracias")
        lblNombre.text = Constants.nombre
        lblNombre.textAlignment = .center;
        
        
        
        let emitterLayer = CAEmitterLayer()
        
        emitterLayer.emitterShape = kCAEmitterLayerLine
        
        
        
        emitterLayer.emitterPosition = CGPoint(x:-300,y:900)
        
        let cell = CAEmitterCell()
        
        cell.birthRate = 10
        
        cell.lifetime = 10
        
        cell.velocity = 2500
        
        cell.scale = 0.1
        
        cell.emissionLongitude = 45
        
        
        
        cell.emissionRange =  (45 * (.pi/180))
        
        print(cell.emissionRange)
        
        cell.contents = UIImage(named: "lineack")!.cgImage
        
        
        
        emitterLayer.emitterCells = [cell]
        
        
        
        //view.layer.insertSublayer(emitterLayer, at: 2)
        
        
        

        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
            self.incio()
        })
        // Do any additional setup after loading the view.
    }

    func incio(){
        let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "home") as! HomeViewController
        
        
        
        DispatchQueue.main.async {
            self.present(photoVC, animated: true, completion: nil)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
