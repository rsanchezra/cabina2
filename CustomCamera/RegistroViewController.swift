import UIKit
import Alamofire
import Spruce
import TextFieldEffects

class RegistroViewController: UIViewController, UIViewControllerTransitioningDelegate, UITextFieldDelegate{
    @IBOutlet weak var fondo3: UIImageView!
    

    let URL_USER_REGISTER = "https://guanajuato.gob.mx/apiCabina/register.php"
    var id_persona = ""
    //View variables
    @IBOutlet weak var instrucciones3: UIImageView!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var lineas3: UIImageView!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var textFieldEdad: UITextField!
    @IBOutlet weak var textFieldCiudad: UITextField!
    @IBOutlet weak var btnSiguiente3: UIButton!
    let transition = CircularTransition()
    
    @IBOutlet weak var tr3: UIImageView!
    @IBOutlet weak var tr2: UIImageView!
    @IBOutlet weak var tr1: UIImageView!
    @IBOutlet weak var imgNombre: UIImageView!
    @IBOutlet weak var siguiente3: UIImageView!

    @IBOutlet weak var mapa3: UIImageView!
    //Button action

    @IBOutlet weak var imgCiudad: UIImageView!
    @IBOutlet weak var imgEdad: UIImageView!
    @IBAction func buttonRegister(_ sender: UIButton) {
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegistroViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func btnSiguiente3N(_ sender: Any) {
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sortFunction = LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.1)
        view.spruce.prepare(with: [.fadeIn, .expand(.slightly)])
        view.spruce.animate([.fadeIn, .expand(.slightly)], sortFunction: sortFunction)


        
        
        
        self.hideKeyboardWhenTappedAround() 
        // Do any additional setup after loading the view, typically from a nib.
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        imgNombre.image = #imageLiteral(resourceName: "nombre")
        imgEdad.image = #imageLiteral(resourceName: "edad")
        imgCiudad.image = #imageLiteral(resourceName: "ciudad")
        tr1.image = nil
        tr2.image = nil
        tr3.image = nil
        let fondo3I: UIImage = UIImage(named: "fondo3")!
        fondo3.image = fondo3I
        let instrucciones3I: UIImage = UIImage(named: "instruccion3")!
        instrucciones3.image = instrucciones3I
        let lineas3I: UIImage = UIImage(named: "lineas3")!
        lineas3.image = lineas3I
        let siguiente3I: UIImage = UIImage(named: "siguiente3")!
        siguiente3.image = siguiente3I
        let mapa3I: UIImage = UIImage(named: "mapas3")!
        mapa3.image = mapa3I
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(tapGesture1))
        siguiente3.addGestureRecognizer(tap1)
        siguiente3.isUserInteractionEnabled = true
        
        let myColor : UIColor = UIColor(red:0.51, green:0.77, blue:0.82, alpha:1.0)
        textFieldEdad.layer.masksToBounds = true
        textFieldEdad.layer.borderColor = myColor.cgColor
        textFieldEdad.layer.borderWidth = 2.0
        textFieldName.layer.masksToBounds = true
        textFieldName.layer.borderColor = myColor.cgColor
        textFieldName.layer.borderWidth = 2.0
        textFieldCiudad.layer.masksToBounds = true
        textFieldCiudad.layer.borderColor = myColor.cgColor
        textFieldCiudad.layer.borderWidth = 2.0
        
        
        textFieldName.setLeftPaddingPoints(30)
        textFieldEdad.setLeftPaddingPoints(30)
        textFieldCiudad.setLeftPaddingPoints(30)
        
        textFieldCiudad.layer.cornerRadius = 15.0
        textFieldEdad.layer.cornerRadius = 15.0
        textFieldName.layer.cornerRadius = 15.0
        
        let paddingView = UIView(frame: CGRectMake(0, 0, 30, 30))
        textFieldName.leftView = paddingView
        textFieldName.leftViewMode = .always
        textFieldName.placeholder = "Nombre"
        let paddingViewE = UIView(frame: CGRectMake(0, 0, 30, 30))
        textFieldEdad.leftView = paddingViewE
        textFieldEdad.leftViewMode = .always
        textFieldEdad.placeholder = "Ciudad"
        let paddingViewC = UIView(frame: CGRectMake(0, 0, 30, 30))
        textFieldCiudad.leftView = paddingViewC
        textFieldCiudad.leftViewMode = .always
        textFieldCiudad.placeholder = "Edad"

        
        textFieldName.delegate = self
        textFieldName.tag = 0
        textFieldName.returnKeyType = UIReturnKeyType.next
        textFieldCiudad.delegate = self
        textFieldCiudad.tag = 1
        textFieldCiudad.returnKeyType = UIReturnKeyType.next
        textFieldEdad.delegate = self
        textFieldEdad.tag = 2
        textFieldEdad.returnKeyType = UIReturnKeyType.go
 
        
        textFieldEdad.addTarget(self, action: #selector(cdtarget(textField:)), for: UIControlEvents.touchDown)
        textFieldCiudad.addTarget(self, action: #selector(edadtarget(textField:)), for: UIControlEvents.touchDown)
        textFieldName.addTarget(self, action: #selector(nombretarget(textField:)), for: UIControlEvents.touchDown)
        
    }
    
    
    @objc func cdtarget(textField: UITextField) {
        tr1.image = nil
        tr2.image = nil
        tr3.image = #imageLiteral(resourceName: "triangulo")
    }
    
    
    @objc func edadtarget(textField: UITextField) {
        tr1.image = nil
        tr2.image = #imageLiteral(resourceName: "triangulo")
        tr3.image = nil
    }
    
    
    @objc func nombretarget(textField: UITextField) {
        tr1.image = #imageLiteral(resourceName: "triangulo")
        tr2.image = nil
        tr3.image = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
            if textField.tag == 0 {
                tr1.image = nil
                tr2.image = #imageLiteral(resourceName: "triangulo")
                tr3.image = nil
            }
            if textField.tag == 1 {
                tr1.image = nil
                tr2.image = nil
                tr3.image = #imageLiteral(resourceName: "triangulo")
            }
        } else {
            textField.resignFirstResponder()
            return true;
        }
        return false
    }
    
    private func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeLeft
    }
    
    private func shouldAutorotate() -> Bool {
        return true
    }
    
    func tapGesture1() {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (textFieldName.text ?? "").isEmpty || (textFieldEdad.text ?? "").isEmpty  || (textFieldCiudad.text ?? "").isEmpty {
            let alert = UIAlertController(title: "Mensaje", message: "Completa los campos por favor", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
        } else {
            //creating parameters for the post request
            let parameters: Parameters=[
                "name":textFieldName.text!,
                "edad":textFieldEdad.text!,
                "ciudad":textFieldCiudad.text!
            ]
            
            
            //Sending http post request
            Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters).responseJSON
                {
                    response in
                    //printing response
                    print(response)
                    
                    //getting the json value from the server
                    if let result = response.result.value {
                        
                        //converting it as NSDictionary
                        let jsonData = result as! NSDictionary
                        
                        //displaying the message in label
                        self.id_persona = (jsonData.value(forKey: "id") as! String?)!
                        Constants.ids = self.id_persona
                        Constants.nombre = self.textFieldName.text!
                    }
            }
            let secondVC = segue.destination as! ViewController
            secondVC.transitioningDelegate = self
            secondVC.modalPresentationStyle = .custom
        }
        
    }
    
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = btnSiguiente3.center
        transition.circleColor = hexStringToUIColor(hex: "#0094A8")
        return transition
    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    


}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
