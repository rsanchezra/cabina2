//
//  CorreoViewController.swift
//  CustomCamera
//
//  Created by MacBook Pro TI on 09/08/18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//

import UIKit
import Alamofire
import Social
import SDWebImage

class CorreoViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    let URL_USER_REGISTER = "https://guanajuato.gob.mx/apiCabina/updateFoto.php"
    

    @IBOutlet weak var fondo6: UIImageView!
    @IBOutlet weak var lineas6: UIImageView!
    @IBOutlet weak var enviar: UIImageView!
    @IBOutlet weak var finalizar: UIImageView!
    @IBOutlet weak var textFieldCorreo: UITextField!
    @IBOutlet weak var quierocompartir: UIImageView!
    @IBOutlet weak var siC: UIImageView!
    @IBOutlet weak var marcosi: UIImageView!
    @IBOutlet weak var separador: UIImageView!
    @IBOutlet weak var marcono: UIImageView!
    @IBOutlet weak var noC: UIImageView!
    @IBOutlet weak var btnFinalizar: UIButton!
    @IBOutlet weak var imgShare: UIImageView!
    let transition = CircularTransition()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        

        enviar.image = #imageLiteral(resourceName: "envia")
        lineas6.image = #imageLiteral(resourceName: "lineas6")
        fondo6.image = #imageLiteral(resourceName: "fondo6")
        finalizar.image = nil
        quierocompartir.image = #imageLiteral(resourceName: "quierocompartir")
        siC.image = #imageLiteral(resourceName: "facebook")
        marcosi.image = nil
        marcono.image = nil
        separador.image = #imageLiteral(resourceName: "separador")
        noC.image = #imageLiteral(resourceName: "twitter")
        textFieldCorreo.layer.borderWidth = 1.0
        let myColor = UIColor.white
        textFieldCorreo.layer.borderColor = myColor.cgColor
        
        
        let nop = UITapGestureRecognizer(target: self, action: #selector(comparte))
        noC.addGestureRecognizer(nop)
        noC.isUserInteractionEnabled = true
        
        let sip = UITapGestureRecognizer(target: self, action: #selector(comparte))
        siC.addGestureRecognizer(sip)
        siC.isUserInteractionEnabled = true
        
    }
    
    
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CorreoViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func comparte() {
        
        let parameters: Parameters=[
            "imagen":Constants.strBase64,
            "marco":Constants.marco_seleccionado,
            "id":Constants.ids,
            "redes": Constants.redes,
            "orientacion": Constants.orientacion
        ]
        
        print("MARCO SELECCIONADO")
        print(Constants.redes)
        
        //Sending http post request
        Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters).responseJSON
            {
                response in
                //printing response
                print(response)
                
                //getting the json value from the server
                
                
                
                let encodedImageData =  Constants.strBase64
                let imageData = NSData(base64Encoded: encodedImageData)
                
                
                self.imgShare.sd_setImage(with: URL(string: "http://guanajuato.gob.mx/apiCabina/tmp/output.png"), placeholderImage: UIImage(named: "placeholder.png"))
                
                // Share text and image
                let activity = UIActivityViewController(activityItems: [self.imgShare, "NADA"], applicationActivities: nil)
                if UIDevice.current.userInterfaceIdiom == .phone {
                    self.present(activity, animated: true, completion: nil)
                }
                
                let share = [self.imgShare, "Museo del Automovil", "http://guanajuato.gob.mx/apiCabina/tmp/output.png"] as [Any]
                let activityViewController = UIActivityViewController(activityItems: share, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
                if let result = response.result.value {
                    
                    //converting it as NSDictionary
                    let jsonData = result as! NSDictionary
                }
        }
        
    }
    func compartetw(){
        Constants.redes = "si"
        marcosi.image = #imageLiteral(resourceName: "simarco")
        marcono.image = nil
    }
    func noredes(){
        Constants.redes = "no"
        marcosi.image = nil
        marcono.image = #imageLiteral(resourceName: "nomarco")
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func tapGesture1() {
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(Constants.ids)
        
            
            
            //creating parameters for the post request
            let parameters: Parameters=[
                "imagen":Constants.strBase64,
                "marco":Constants.marco_seleccionado,
                "id":Constants.ids,
                "correo":textFieldCorreo.text!,
                "redes": Constants.redes,
                "orientacion": Constants.orientacion
            ]
            
            print("MARCO SELECCIONADO")
            print(Constants.redes)
            
            //Sending http post request
            Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters).responseJSON
                {
                    response in
                    //printing response
                    print(response)
                    
                    //getting the json value from the server
                    if let result = response.result.value {
                        
                        //converting it as NSDictionary
                        let jsonData = result as! NSDictionary
                    }
            }
            
            
            let secondVC = segue.destination as! EndViewController
            secondVC.transitioningDelegate = self
            secondVC.modalPresentationStyle = .custom
            
        /*} else {
            let alert = UIAlertController(title: "Mensaje", message: "Ingresa un correo valido", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
        }*/
    }
    
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = btnFinalizar.center
        transition.circleColor = hexStringToUIColor(hex: "#0094A8")
        return transition
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    

}
