//
//  HomeViewController.swift
//  CustomCamera
//
//  Created by MacBook Pro TI on 15/08/18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//
import UIKit
import Spruce
import Motion
import Reachability

class HomeViewController: UIViewController, UIViewControllerTransitioningDelegate {
    @IBOutlet weak var fondo1: UIImageView!
    @IBOutlet weak var lineas1: UIImageView!
    @IBOutlet weak var circulo: UIImageView!
    @IBOutlet weak var logo1: UIImageView!
    @IBOutlet weak var logoH: UIImageView!
    @IBOutlet weak var toca: UIImageView!
    @IBOutlet weak var logoHome: UIImageView!
    let transition = CircularTransition()
    let reachability = Reachability()!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isMotionEnabled = true
        fondo1.image = #imageLiteral(resourceName: "fondo7")
        //lineas1.image = #imageLiteral(resourceName: "lineas7")
        circulo.image = #imageLiteral(resourceName: "circulo")
        logo1.image = #imageLiteral(resourceName: "logo1")
        logoHome.image = #imageLiteral(resourceName: "logo1")
        toca.image = #imageLiteral(resourceName: "toca")
        
        
        
        let emitterLayer = CAEmitterLayer()

        emitterLayer.emitterShape = kCAEmitterLayerLine
        
        
        
        emitterLayer.emitterPosition = CGPoint(x:-300,y:900)
        
        let cell = CAEmitterCell()
        
        cell.birthRate = 10
        
        cell.lifetime = 10
        
        cell.velocity = 2500
        
        cell.scale = 0.1
        
        cell.emissionLongitude = 45
        
        
        
        cell.emissionRange =  (45 * (.pi/180))
        
        print(cell.emissionRange)
        
        cell.contents = UIImage(named: "lineack")!.cgImage
        
        
        
        emitterLayer.emitterCells = [cell]
        
        
        
        //view.layer.insertSublayer(emitterLayer, at: 2)
        

        
        let sortFunction = LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.1)
        
        
        self.view.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.showHideTransitionViews, animations: { () -> Void in
            self.view.alpha = 1
        }, completion: { (Bool) -> Void in    }
        )
        
        
        self.toca.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.showHideTransitionViews, animations: { () -> Void in
            self.toca.alpha = 1
        }, completion: { (Bool) -> Void in    }
        )
        
        
        self.logoHome.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.showHideTransitionViews, animations: { () -> Void in
            self.logoHome.alpha = 1
        }, completion: { (Bool) -> Void in    }
        )
        
        
        self.fondo1.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.showHideTransitionViews, animations: { () -> Void in
            self.fondo1.alpha = 1
        }, completion: { (Bool) -> Void in    }
        )
        
        
        self.circulo.alpha = 0
        UIView.animate(withDuration: 01.5, delay: 0, options: [UIViewAnimationOptions.showHideTransitionViews, .repeat, .autoreverse], animations: { () -> Void in
            self.circulo.alpha = 1
        }, completion: { (Bool) -> Void in    }
        )

        
        
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
                self.taps()
            } else {
                print("Reachable via Cellular")
                self.taps()
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            let alert = UIAlertController(title: "Mensaje", message: "No hay conexión activa de internet", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondVC = segue.destination as! ImageViewController
        secondVC.transitioningDelegate = self
        secondVC.modalPresentationStyle = .custom
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = logoHome.center
        transition.circleColor = logoHome.backgroundColor!
        
        return transition
    }

    func siguiente(){
        let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "inicio") as! ImageViewController
        
        DispatchQueue.main.async {
            self.present(photoVC, animated: true, completion: nil)
            
        }
    }
    
    func taps(){
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.siguiente))
        self.toca.addGestureRecognizer(tap1)
        self.toca.isUserInteractionEnabled = true
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.siguiente))
        self.logoHome.addGestureRecognizer(tap2)
        self.logoHome.isUserInteractionEnabled = true
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.siguiente))
        self.circulo.addGestureRecognizer(tap3)
        self.circulo.isUserInteractionEnabled = true
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.siguiente))
        self.lineas1.addGestureRecognizer(tap4)
        self.lineas1.isUserInteractionEnabled = true
    }
    
    

}

