//
//  ImageViewController.swift
//  CustomCamera
//
//  Created by MacBook Pro TI on 07/08/18.
//  Copyright © 2018 Brian Advent. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UIViewControllerTransitioningDelegate {

    @IBOutlet weak var instrucciones: UIImageView!
    
    @IBOutlet weak var lineas: UIImageView!
    @IBOutlet weak var marco: UIImageView!
    @IBOutlet weak var titulo: UIImageView!
    @IBOutlet weak var uno: UIImageView!
    @IBOutlet weak var dos: UIImageView!
    @IBOutlet weak var tres: UIImageView!
    @IBOutlet weak var btnSiguiente: UIButton!
    let transition = CircularTransition()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addImageToUIImageView()
        // Do any additional setup after loading the view.
    }
    
    func tapGesture1() {
        let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "registro") as! RegistroViewController
        

        
        DispatchQueue.main.async {
            self.present(photoVC, animated: true, completion: nil)
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondVC = segue.destination as! RegistroViewController
        secondVC.transitioningDelegate = self
        secondVC.modalPresentationStyle = .custom
    }
    
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = btnSiguiente.center
        transition.circleColor = hexStringToUIColor(hex: "#0094A8")
        return transition
    }
    
    func addImageToUIImageView(){
        let fondo: UIImage = UIImage(named: "fondo")!
        instrucciones.image = fondo
        let lineasI: UIImage = UIImage(named: "lineas")!
        lineas.image = lineasI
        let marcoI: UIImage = UIImage(named: "marco_instrucciones")!
        marco.image = marcoI
        let tituloI: UIImage = UIImage(named: "Instrucciones")!
        titulo.image = tituloI
        let unoI: UIImage = UIImage(named: "1")!
        uno.image = unoI
        let dosI: UIImage = UIImage(named: "2")!
        dos.image = dosI
        let tresI: UIImage = UIImage(named: "3")!
        tres.image = tresI

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
